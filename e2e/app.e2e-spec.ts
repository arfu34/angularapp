import { HelloAngularCliPage } from './app.po';

describe('hello-angular-cli App', () => {
  let page: HelloAngularCliPage;

  beforeEach(() => {
    page = new HelloAngularCliPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
