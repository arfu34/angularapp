import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule, MdDialogModule  } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http'

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { MainComponent } from './main/main.component';
import { MembersComponent } from './members/members.component';
import { ModalformComponent } from './modalform/modalform.component';
import { DebtsComponent } from './debts/debts.component'
import { TransactionsComponent } from './transactions/transactions.component';

import { ModalService } from './services/modal-service.service'
import { MemberService } from './services/member-service.service';
import { DebtsService } from './services/debts-service.service';
import { TransactionService } from './services/transaction-service.service';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    MainComponent,
    MembersComponent,
    ModalformComponent,
    DebtsComponent,
    TransactionsComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpModule,
    ReactiveFormsModule,
    MaterialModule,
    MdDialogModule
  ],
  entryComponents: [
    ModalformComponent
  ],
  providers: [
    ModalService,
    MemberService,
    DebtsService,
    TransactionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
