import { Component, OnInit } from '@angular/core';
import { DebtsService } from '../services/debts-service.service'

@Component({
  selector: 'app-debts',
  templateUrl: './debts.component.html',
  styleUrls: ['./debts.component.css']
})
export class DebtsComponent implements OnInit {

  constructor(private debtService : DebtsService) { }

  debtsList : any[];

  ngOnInit() {
    this.debtsList = this.debtService.GetDebts(1);
  }

}
