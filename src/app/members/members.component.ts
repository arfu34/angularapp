import { Component, OnInit, Input } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MdDialog, MdDialogRef } from '@angular/material';

import { ModalService } from '../services/modal-service.service'
import { MemberService } from '../services/member-service.service'

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css']
})
export class MembersComponent implements OnInit {

  dialogRef: MdDialogRef<any>;

  membersList : Array<any>;

  constructor(public dialog: MdDialog, private memberService: MemberService, private modalService: ModalService) { 
    this.modalService.MembersChangedEvent.subscribe(i => this.RefreshList(i));
  }

  RefreshList(item: {}){
    //TODO: enable this when RESTAPI is used
    //this.membersList.push(item);  
  }

  ngOnInit() {
    this.membersList = this.memberService.GetMembers(2);
  }

  OpenModalForm() {
    this.modalService.OpenModalFormComponent();
  }

  DeleteMember(index: number) {
    this.memberService.RemoveMember(this.membersList[index], index);
  }
}
