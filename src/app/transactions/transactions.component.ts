import { Component, OnInit } from '@angular/core';
import { TransactionService } from '../services/transaction-service.service'

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit {

  transactionsList;

  constructor(private transactionService: TransactionService) { 

  }

  ngOnInit() {
    var subs = this.transactionService.GetTransactions(1);
    subs.subscribe(resp => this.transactionsList = resp)
  }

}
