import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  groupsArray = ['Action', 'Another action'];
  constructor() {  }

  ngOnInit() {
  }

  CreateNewGroup(){
    console.log(this.groupsArray);
  }

  SelectGroup(index){
    console.log(this.groupsArray[index]);
  }
}