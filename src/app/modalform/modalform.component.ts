import { Component } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';

import { MemberService } from '../services/member-service.service'

@Component({
  selector: 'app-modalform',
  templateUrl: './modalform.component.html',
  styleUrls: ['./modalform.component.css']
})
export class ModalformComponent {

  name : FormControl;
  email : FormControl;
  myform: FormGroup;
  isEnabled = false;

  dialogRefComp: MdDialogRef<ModalformComponent>;
  constructor(public dialogRef: MdDialogRef<ModalformComponent>, private memberService: MemberService) { 
    this.dialogRefComp = dialogRef;
  }

  ngOnInit() {
    this.createFormControls();
    this.createForm();
  }

  createFormControls() {
    this.name = new FormControl('', Validators.required);
    this.email = new FormControl('', Validators.required);
  }

  createForm() {
    this.myform = new FormGroup({
      name: this.name,
      email: this.email
    });
  }

  Close() {
    console.log(this.name.invalid && (this.name.dirty || this.name.touched));
    console.log(this.email.invalid);
    this.dialogRefComp.close("");
  }

  SaveMember() {
    if (this.myform.valid)
    {
      this.memberService.AddMember(this.myform.value);
      this.dialogRef.close(this.myform.value);
    }
  }
}
