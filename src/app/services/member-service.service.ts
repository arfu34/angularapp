import { Injectable } from '@angular/core';

@Injectable()
export class MemberService {

  constructor() { }

  membersArray: any[] = [
    { "name": "Mohd Arfain", "email": "arf@live.in" },
    { "name": "Noman", "email": "nom@live.in" },
    { "name": "H", "email": "h@live.in" }
  ];

  GetMembers(groupId: number) {
    return this.membersArray;
  }

  AddMember(json: String){
    this.membersArray.push(json);
  }

  RemoveMember(obj : {}, index: number){
    this.membersArray.splice(index, 1);
  }
}
