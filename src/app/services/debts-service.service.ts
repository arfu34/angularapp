import { Injectable } from '@angular/core';

@Injectable()
export class DebtsService {

  debtsList: any[] = [
    { "inUser": "Mohd Arfain", "ofUser": "Noman", "amount":"100" },
    { "inUser": "Mohd Arfain", "ofUser": "Noman", "amount":"20" },
    { "inUser": "Mohd Arfain", "ofUser": "Noman", "amount":"50" },
    { "inUser": "Mohd Arfain", "ofUser": "Noman", "amount":"30" },
    { "inUser": "Mohd Arfain", "ofUser": "Noman", "amount":"10" },
    { "inUser": "Mohd Arfain", "ofUser": "Noman", "amount":"0.5" }
  ];

  constructor() { }

  GetDebts(groupId : number){
    return this.debtsList;
  }

}
