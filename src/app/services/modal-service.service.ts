import { Injectable, EventEmitter } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';

import { ModalformComponent } from '../modalform/modalform.component';

@Injectable()
export class ModalService {

  dialogRef: MdDialogRef<any>;
  public MembersChangedEvent: EventEmitter<{}>;

  constructor(public dialog: MdDialog) {
    this.MembersChangedEvent = new EventEmitter();
   }

  public OpenModalFormComponent() {
    this.dialogRef = this.dialog.open(ModalformComponent).updateSize("200", "220");
    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.MembersChangedEvent.emit(result);
      }
    });
  }

}
