import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class TransactionService {
  
  private transactionsArray : any[] = [
    { "paidByWho": "Mohd Arfain", "amountPaid": "25", "paidFor" : "Noman, Heena" },
    { "paidByWho": "Mohd Arfain", "amountPaid": "25", "paidFor" : "Noman, Heena" },
    { "paidByWho": "Mohd Arfain", "amountPaid": "25", "paidFor" : "Noman, Heena" },
    { "paidByWho": "Mohd Arfain", "amountPaid": "25", "paidFor" : "Noman, Heena" },
    { "paidByWho": "Mohd Arfain", "amountPaid": "25", "paidFor" : "Noman, Heena" },
    { "paidByWho": "Mohd Arfain", "amountPaid": "25", "paidFor" : "Noman, Heena" },
    { "paidByWho": "Mohd Arfain", "amountPaid": "25", "paidFor" : "Noman, Heena" },
    { "paidByWho": "Mohd Arfain", "amountPaid": "25", "paidFor" : "Noman, Heena" }   
  ] 

  constructor(private httpService: Http) {

   }

  GetTransactions(groupId: number){
    return this.httpService.get('assets/transactions.json')
    .map(response => response.json())
  }

}
